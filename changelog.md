# Changelog

Todas as alterações do Sistema Conta serão documentadas neste arquivo.

## [5.0.001] Próxima - Previsão: 31/07/2019

### Implementação
- CRUD para alteração do Instrumento Inicial para correção de erros digitação;
- Data Início e Data Fim para Responsáveis de Contratos;
- Cronograma de Desembolso Financeiro do Contrato;
- Todas as funcionalidades do Sistema Conta Versão 4.1.11 (Framework Scriptcase);

### Descontinuada
- Nenhuma

### Bug
- Correção do cálculo do Valor Acumulado atualizado por meio do Cronograma (bug da versão 4.1.11).
- Alteração do campo receita_despesa da tabela contratohistorico para CHAR(1).

### Removida
- Nenhuma

### Segurança
- Nenhuma

-----------

## [5.0.000] - 15/03/2019 - Versão de Sistema em Desenvolvimento

### Implementação - em Andamento
- Algumas as funcionalidades do Sistema Conta Versão 4.1.11 (Framework Scriptcase);

### Bug
- Nenhuma.
